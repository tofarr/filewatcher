/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tofarr.filewatcher;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tofarrell
 */
public class LogMissingVisitor implements FileVisitor<Path> {

    private static final Logger LOG = Logger.getLogger(LogMissingVisitor.class.getName());
    private final CopyMapping mapping;

    public LogMissingVisitor(CopyMapping mapping) throws NullPointerException {
        if (mapping == null) {
            throw new NullPointerException();
        }
        this.mapping = mapping;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        Path dest = mapping.map(dir);
        if (dest == null) {
            LOG.log(Level.WARNING, "Could not map directory: {0}", dir);
            return FileVisitResult.SKIP_SUBTREE;
        } else if (Files.exists(dest)) {
            return FileVisitResult.CONTINUE;
        } else {
            LOG.log(Level.WARNING, "Missing directory: {0}", dir);
            return FileVisitResult.SKIP_SUBTREE;
        }
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        Path dest = mapping.map(file);
        if (dest == null) {
            LOG.log(Level.WARNING, "Could not map file: {0}", file);
        } else if (!Files.exists(dest)) {
            LOG.log(Level.WARNING, "Missing file: {}", file);
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
        LOG.log(Level.SEVERE, "Error visiting file: "+file, exc);
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        return FileVisitResult.CONTINUE;
    }
}
