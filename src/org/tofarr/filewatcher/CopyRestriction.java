package org.tofarr.filewatcher;

import java.nio.file.Path;

/**
 *
 * @author tofarrell
 */
public interface CopyRestriction {

    public boolean allow(Path path);
    
    
    public static final CopyRestriction NO_RESTRICTION = new CopyRestriction(){

        @Override
        public boolean allow(Path path) {
            return true;
        }
    
    };
}
