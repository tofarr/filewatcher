package org.tofarr.filewatcher;

import java.nio.file.Path;

/**
 *
 * @author tofarrell
 */
public class DefaultMapping implements CopyMapping {

    private final Path local;
    private final Path remote;

    public DefaultMapping(Path local, Path remote) throws NullPointerException {
        if ((local == null) || (remote == null)) {
            throw new NullPointerException();
        }
        this.local = local.toAbsolutePath();
        this.remote = remote.toAbsolutePath();
    }

    @Override
    public Path map(Path path) {
        return map(path, local, remote);
    }
    
    @Override
    public CopyMapping invert(){
        return new DefaultMapping(remote, local);
    }
    
    static Path map(Path path, Path from, Path to){
        if (path.startsWith(from)) {
            return to.resolve(from.relativize(path));
            //String subpath = source.toString().substring(local.toAbsolutePath().toString().length());
            //Path dest = remote.getFileSystem().getPath(remote.toString() + subpath);
            //return dest;
        }else{
            return null;
        }
    }
}
