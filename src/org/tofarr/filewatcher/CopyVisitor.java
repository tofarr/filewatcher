package org.tofarr.filewatcher;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tofarrell
 */
public class CopyVisitor implements FileVisitor<Path> {

    private static final Logger LOG = Logger.getLogger(CopyVisitor.class.getName());
    private final CopyMapping mapping;
    private final CopyRestriction restriction;

    public CopyVisitor(CopyMapping mapping, CopyRestriction restriction) throws NullPointerException {
        if (mapping == null) {
            throw new NullPointerException();
        }
        this.mapping = mapping;
        this.restriction = (restriction == null) ? CopyRestriction.NO_RESTRICTION : restriction;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        if (restriction.allow(dir)) { //SKIP CVS DIRECTORIES!!!
            Path destDir = mapping.map(dir);
            if (Files.exists(destDir)) {
                if (!Files.isDirectory(destDir)) {
                    LOG.log(Level.WARNING, "Cannot map directory onto file : {0}", destDir);
                }
            } else {
                Files.createDirectory(destDir);
            }
            return FileVisitResult.CONTINUE;
        } else {
            return FileVisitResult.SKIP_SUBTREE;
        }
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if(restriction.allow(file)){
            Path dest = mapping.map(file);
            if((!Files.exists(dest))
                || (Files.size(dest) != Files.size(file)) 
                || (Files.getLastModifiedTime(dest).compareTo(Files.getLastModifiedTime(file)) < 0)){
                LOG.log(Level.INFO, "Copying File: {0}", file);
                Files.copy(file, dest, StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
            }
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
        LOG.log(Level.WARNING, "Visit Failed: "+file, exc);
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        //String fileName = dir.getFileName().toString();
        //if (!"CVS".equals(fileName)) { //SKIP CVS DIRECTORIES!!!
        //    Files.walkFileTree(mapping.map(dir), new LogMissingVisitor(mapping.invert()));
        //}
        return FileVisitResult.CONTINUE;
    }
}
