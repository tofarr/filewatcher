package org.tofarr.filewatcher;

import java.nio.file.Path;

/**
 *
 * @author tofarrell
 */
public class StandardRestriction implements CopyRestriction {

    @Override
    public boolean allow(Path path) {
        String fileName = path.getFileName().toString();
        return (!"CVS".equals(fileName)) && (!"WEB-INF".equals(fileName));
    }
    
}
