package org.tofarr.filewatcher;

import java.nio.file.Path;

/**
 *
 * @author tofarrell
 */
public interface CopyMapping {

    /**
     * Map a source from source to destination - return null if no mapping could be performed
     * @param path path
     * @return mapped path, or null if mapping was not possible
     */
    public Path map(Path path);
    
    public CopyMapping invert();
}
