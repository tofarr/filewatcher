package org.tofarr.filewatcher;

import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tofarrell
 */
public class CopyWatcher implements Runnable {

    private static final Logger LOG = Logger.getLogger(CopyWatcher.class.getName());
    private final Path source;
    private final Path dest;
    private final CopyMapping mapping;
    private final CopyRestriction restriction;
    private final WatchService watcher;
    private final Map<WatchKey, Path> keys;
    private volatile boolean doRun;
    private volatile boolean running;

    public CopyWatcher(Path source, Path dest, CopyRestriction restriction) throws IOException {
        this.source = source;
        this.dest = dest;
        mapping = new DefaultMapping(source, dest);
        this.restriction = (restriction == null) ? CopyRestriction.NO_RESTRICTION : restriction;
        watcher = FileSystems.getDefault().newWatchService();
        this.keys = new HashMap<>();
    }

    /**
     * Register the given directory with the WatchService
     */
    private void register(Path dir) throws IOException {
        WatchKey key = dir.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
        if (LOG.isLoggable(Level.INFO)) {
            Path prev = keys.get(key);
            if (prev == null) {
                LOG.info(MessageFormat.format("register: {0}", dir));
            } else {
                if (!dir.equals(prev)) {
                    LOG.info(MessageFormat.format("update: {0} -> {1}", prev, dir));
                }
            }
        }
        keys.put(key, dir);
    }

    /**
     * Register the given directory, and all its sub-directories, with the WatchService.
     */
    private void registerAll(final Path start) throws IOException {
        // register directory and sub-directories
        Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                if(restriction.allow(dir)){
                    register(dir);
                    return FileVisitResult.CONTINUE;
                }else{
                    return FileVisitResult.SKIP_SUBTREE;
                }
            }
        });
    }

    private void copyPath(Kind kind, Path source) {
        try {
            if(restriction.allow(source)){
                if ((kind == ENTRY_CREATE) || (kind == ENTRY_MODIFY)) {
                    Path dest = mapping.map(source);
                    if (dest != null) {
                        if((!Files.exists(dest)) || (Files.getLastModifiedTime(source).compareTo(Files.getLastModifiedTime(dest)) > 0)) {
                            LOG.log(Level.INFO, "Copying file: {0}", source);
                            Files.copy(source, dest, StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
                        }
                    }
                } else if (kind == ENTRY_DELETE) {
                    Path dest = mapping.map(source);
                    if (dest != null) {
                        LOG.log(Level.INFO, "Deleting file: {0}", source);
                        Files.delete(dest);
                    }
                }
            }
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }

    private synchronized boolean shouldRun() {
        return doRun;
    }

    @SuppressWarnings("unchecked")
    static <T> WatchEvent<T> cast(WatchEvent<?> event) {
        return (WatchEvent<T>) event;
    }

    @Override
    public void run() {
        try {
            synchronized (this) {
                if (running) {
                    return;
                }
                doRun = running = true;
            }
            Files.walkFileTree(source, new CopyVisitor(mapping, restriction));
            registerAll(source);

            while (shouldRun()) {

                // wait for key to be signalled
                WatchKey key;
                try {
                    key = watcher.take();
                } catch (InterruptedException x) {
                    return;
                }

                Path dir = keys.get(key);
                if (dir == null) {
                    LOG.severe(MessageFormat.format("WatchKey not recognized: {}", key));
                    continue;
                }

                for (WatchEvent<?> event : key.pollEvents()) {
                    Kind kind = event.kind();

                    // TBD - provide example of how OVERFLOW event is handled
                    if (kind == OVERFLOW) {
                        continue;
                    }

                    // Context for directory entry event is the file name of entry
                    WatchEvent<Path> ev = cast(event);
                    Path name = ev.context();
                    Path child = dir.resolve(name);

                    //Copy files here
                    copyPath(kind, child); // sync files

                    // if directory is created, and watching recursively, then
                    // register it and its sub-directories
                    if (kind == ENTRY_CREATE) {
                        try {
                            if (Files.isDirectory(child, NOFOLLOW_LINKS)) {
                                registerAll(child);
                            }
                        } catch (IOException x) {
                            // ignore to keep sample readbale
                        }
                    }
                }

                // reset key and remove from set if directory no longer accessible
                boolean valid = key.reset();
                if (!valid) {
                    keys.remove(key);

                    // all directories are inaccessible
                    if (keys.isEmpty()) {
                        break;
                    }
                }
            }
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        synchronized (this) {
            running = false;
            notifyAll();
        }
    }

    public synchronized void stop() {
        doRun = false;
        while (running) {
            try {
                wait();
            } catch (InterruptedException ex) {
                LOG.log(Level.SEVERE, null, ex);
            }
        }
    }
}
