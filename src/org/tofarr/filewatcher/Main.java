package org.tofarr.filewatcher;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 *
 * @author tofarrell
 */
public class Main {

    public static void main(String[] args) throws IOException {
        
        File configFile = new File((args.length > 0) ? args[0] : "config/settings.json");
        JsonObject config = new JsonParser().parse(new BufferedReader(new FileReader(configFile))).getAsJsonObject();
        
        for(JsonElement element : config.getAsJsonArray("watchers")){
            JsonObject watcherConfig = element.getAsJsonObject();
            Path src = Paths.get(watcherConfig.getAsJsonPrimitive("src").getAsString());
            Path dst = Paths.get(watcherConfig.getAsJsonPrimitive("dst").getAsString());
            CopyRestriction restriction = watcherConfig.has("standardRestriction") && watcherConfig.getAsJsonPrimitive("standardRestriction").getAsBoolean() ? new StandardRestriction() : null;
            CopyWatcher watcher = new CopyWatcher(src, dst, restriction);
            new Thread(watcher).start();
        }
        
        //CopyWatcher watcher = new CopyWatcher(Paths.get("C:\\tmp\\syncTest\\src"),Paths.get("C:\\tmp\\syncTest\\dst"));
        //new Thread(watcher).start();
        
//        // parse arguments
//        if (args.length == 0 || args.length > 2) {
//            usage();
//        }
//        boolean recursive = false;
//        int dirArg = 0;
//        if (args[0].equals("-r")) {
//            if (args.length < 2) {
//                usage();
//            }
//            recursive = true;
//            dirArg++;
//        }
//
//        // register directory and process its events
//        Path dir = Paths.get(args[dirArg]);
//        new WatchDir(dir, recursive).processEvents();
        
//        Path dir = Paths.get("C:\\tmp\\datasets");
//        System.out.println(dir.getFileName().toString());
//        System.out.println(dir.getFileName().toAbsolutePath());
//
//        Path dir2 = dir.resolve("quakes");
//        System.out.println(dir2.toString());
//        System.out.println(dir2.toAbsolutePath());
//        
//        Path dir3 = Paths.get("C:\\dev");
//        System.out.println(dir3.relativize(dir2).toString());
//        System.out.println(dir3.relativize(dir2).toAbsolutePath());
//        System.out.println(dir3.resolve(dir2).toString());
//        System.out.println(dir3.resolve(dir2).toAbsolutePath());
//        System.out.println(dir3.resolve(dir2.getFileName()).toString());
//        System.out.println(dir3.resolve(dir2.getFileName()).toAbsolutePath());
    }
}
